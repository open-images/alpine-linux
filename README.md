<div id="top"></div>

<!-- PROJECT SHIELDS -->
[![Contributors][contributors-shield]][contributors-url]
[![Builds][builds-shield]][builds-url]
[![Versions][versions-shield]][versions-url]
[![Starrers][stars-shield]][stars-url]
[![Forks][forks-shield]][forks-url]
[![Issues][issues-shield]][issues-url]


<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="[https://gitlab.com/open-images/alpine-linux](https://gitlab.com/open-images/alpine-linux)">
    <img src="images/logo.svg" alt="Logo" width="250">
  </a>

<h3 align="center">Alpine Linux image for OpenStack</h3>

  <p align="center">
    Port of Alpine Linux distribution for OpenStack environments
    <br />
    <a href="https://gitlab.com/open-images/alpine-linux"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/open-images/alpine-linux/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/open-images/alpine-linux/issues">Request Feature</a>
  </p>
</div>

<!-- ABOUT THE PROJECT -->
## About The Project

This project is a port of Alpine Linux releases for OpenStack environments.  
This image used the the project [alpine-make-vm-image](https://github.com/alpinelinux/alpine-make-vm-image "alpine-make-vm-image project") to build base image and we make some change to make it compatible with OpenStack environments and cloud-init.  

This image is updated when Alpine Linux team released a new version of the OS [here](https://alpinelinux.org/releases/ "Alpine linux Release Inventory").

<div style="text-align: right"><p align="right">(<a href="#top">back to top</a>)</p></div>

### How to use this image

1. Set your OpenStack environement variables
2. Download the latest image from the [repository page](https://s3.openimages.cloud/alpine-image/index.html "Images Repository")
3. Upload image to your OpenStack environment

   ```bash
   openstack image create --disk-format=qcow2 --container-format=bare --file alpine-<VERSION>-x86_64.qcow2  'Alpine <VERSION>'
   ```
4. You can connect to your instance with SSH using your private key. The username is **alpine**.

<div style="text-align: right"><p align="right">(<a href="#top">back to top</a>)</p></div>

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<div style="text-align: right"><p align="right">(<a href="#top">back to top</a>)</p></div>



<!-- LICENSE -->
## License

Distributed under the GPL-3.0 License. See `LICENSE.md` for more informations.

<div style="text-align: right"><p align="right">(<a href="#top">back to top</a>)</p></div>



<!-- CONTACT -->
## Contact

Kevin Allioli - [@linit_io](https://twitter.com/linit_io) - kevin@linit.io.  
Project Link: https://gitlab.com/open-images/alpine-linux

<div style="text-align: right"><p align="right">(<a href="#top">back to top</a>)</p></div>


<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/gitlab/contributors/open-images/alpine-linux.svg?style=for-the-badge
[contributors-url]: https://gitlab.com/open-images/alpine-linux/-/graphs/main
[builds-shield]: https://img.shields.io/gitlab/pipeline-status/open-images/alpine-linux.svg?style=for-the-badge
[builds-url]: https://gitlab.com/open-images/alpine-linux/-/pipelines
[versions-shield]: https://img.shields.io/gitlab/v/tag/open-images/alpine-linux?style=for-the-badge&label=Latest%20version
[versions-url]: https://gitlab.com/open-images/alpine-linux/-/tags?sort=version_desc
[stars-shield]: https://img.shields.io/gitlab/stars/open-images/alpine-linux.svg?style=for-the-badge
[stars-url]: https://gitlab.com/open-images/alpine-linux/-/starrers
[forks-shield]: https://img.shields.io/gitlab/forks/open-images/alpine-linux?style=for-the-badge
[forks-url]: https://gitlab.com/open-images/alpine-linux/-/forks
[issues-shield]: https://img.shields.io/gitlab/issues/open/open-images/alpine-linux.svg?style=for-the-badge
[issues-url]: https://gitlab.com/open-images/alpine-linux/-/issues